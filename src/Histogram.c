/*
 * Histogram.c
 *
 *  Created on: Jul 27, 2018
 *      Author: infolab
 */

/***************************** Include Files *********************************/
#include "Histogram.h"
#include <stdio.h>
#include <stdlib.h>
#include "xil_io.h"

/************************** Variable Definitions *****************************/

/*****************************************************************************/
/**
 * This function initialize the Histogram block and clean RAM memory befor using
 *
 * @param ptr histo: histogram pointer 
 *  @param histo_cfg_addr; u32 Configuration register address
 *  @param histo_setN_reg_addr; u32 Set N input register address
 *  @param histo_currCount_addr; u32 Current count output register address
 *  @param histo_done_addr; u32 Done signal address
 *  @param histo_RAM_baseaddr; u32 Memory base address
 *  @param Memory_bins; u32 Maximum number of bins
 * @return status HST_SUCCESS, HST_FAILURE
 *
 *****************************************************************************/

int Histo_init(histo *ptr, u32 CFG_ADDR, u32 setN_ADDR, u32 currCount_addr, u32 Done_addr, u32 RAM_BASEADDR, u32 BINS){
	ptr->histo_cfg_addr=CFG_ADDR;
	ptr->histo_setN_reg_addr=setN_ADDR;
	ptr->histo_currCount_addr=currCount_addr;
	ptr->histo_done_addr=Done_addr;
	ptr->histo_RAM_baseaddr=RAM_BASEADDR;
	ptr->Memory_bins=BINS;
	xil_printf("Cleaning memory\n\r");
	clean_TDPR(ptr);
	Xil_Out32(CFG_ADDR, 1); //Enable Histogram
	return 1;
}


/*****************************************************************************/
/**
 * This function sets the histogram counter register in the DRIVER. 
 *
 * @param   ptr Histogram control pointer
 * @param	N Number of counts for the histogram to generate
 * @return
 * 		- HST_SUCCESS
 *
 *****************************************************************************/

int Histo_SetCounts(histo *ptr, u32 N){
	// xil_printf("Setting Count number as %l",val);
	Xil_Out32(ptr->histo_setN_reg_addr, N); //Send max_count_value to histogram
	Xil_Out32(ptr->histo_cfg_addr, 0); //Reset portB outpus and reseting count and setting max_value
	Xil_Out32(ptr->histo_cfg_addr, 1); //Counter rst_signal ready
	return HST_SUCCESS;
}

/*****************************************************************************/
/**
 * This functions reads the current count number in the histogram. 
 *
 * @param   ptr Histogram control pointer
 * @return
 * 		- u32 Count number
 *
 ******************************************************************************/
u32 Histo_Get_Curr_Counts(histo *ptr){
	int val= Xil_In32(ptr->histo_currCount_addr);
	verbose("Current Count is %d",val);
	return val;

}

/*****************************************************************************/
/**
 * This functions enable the histogram counts
 *
 * @param   ptr Histogram control pointer
 *
 * @return CTRL_REGISTER writen value
 ******************************************************************************/

int Histo_count_enable(histo *ptr, u32 en){
	u32 cfg = Xil_In32(ptr->histo_cfg_addr);
	verbose("Old Configuration %d \n\r",cfg);
	u32 new_cfg=(cfg | (en<<1 & cfg_enable)); //sets enable value
	Xil_Out32(ptr->histo_cfg_addr,new_cfg);
	verbose("New config %d \n\r",new_cfg);
	return new_cfg;
}

/*****************************************************************************/
/**
 * Check DONE flag on the histogram
 *
 * @param   ptr Histogram control pointer
 *
 * @return DONE signal
 ******************************************************************************/

int Histo_is_done(histo *ptr){
	u32 din=Xil_In32(ptr->histo_done_addr);
	verbose("Is done register %d", din);
	return din;
}

/*****************************************************************************/
/**
 * Histogram Read from memory ONE memory location at a time and write it in the
 * trans_buff
 *
 * @param   ptr Histogram control pointer
 * @param 	trans_buff transmission buffer pointer
 *
 * @return HST_SUCCESS or HST_FAILURE
 ******************************************************************************/

int Histo_rfm(histo *ptr, u32 *trans_buf){ //Read from memory
	int status=Histo_is_done(ptr);
	verbose("Status is %d \n\r", status);
	for (int addr=0; addr<=ptr->Memory_bins*4; addr+=4){
		trans_buf[addr/4]=Xil_In32(ptr->histo_RAM_baseaddr+addr);
	}
//		trans_buf=ptr->histo_RAM_baseaddr;
		return status;
}



/*****************************************************************************/
/**
 * This cleans by software the RAM memory connected to the histogram.
 *
 * @param	ptr Histogram pointer 
 * @return
 * 		- ADC500 status register
 *
 *****************************************************************************/
int clean_TDPR(histo *ptr){
	for (int addr=0; addr<=ptr->Memory_bins*4; addr+=4){
		Xil_Out32(ptr->histo_RAM_baseaddr+addr,0);
	}
	return HST_SUCCESS;
}

