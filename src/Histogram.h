/*
 * Histogram.h
 *
 *  Created on: Jul 27, 2018
 *      Author: infolab
 */

#ifndef SRC_HISTOGRAM_H_
#define SRC_HISTOGRAM_H_

/***************************** Include Files *********************************/
#include "xparameters.h"
#include <stdio.h>
#include <stdlib.h>
#include "xil_io.h"
#include "verbose.h"

/**************************** HISTOGRAM REGISTERS DEFINITIONS *****************/

#define cfg_rst 1
#define cfg_enable 2
// #define MEMORY_BINS	0xFFFFU //65535



/**************************** STRUCTURE DEFINITION *****************/

typedef struct{
    u32 histo_cfg_addr; //Configuration register address
    u32 histo_setN_reg_addr; //Set N input register address
    u32 histo_currCount_addr; //Current count output register address
    u32 histo_done_addr; //Done signal address
    u32 histo_RAM_baseaddr; //Memory base address
    u32 Memory_bins; //Maximum number of bins
}histo;

histo pAmp_histo;

#define HST_SUCCESS                     0L
#define HST_FAILURE                     1L

int Histo_init(histo *ptr, u32 CFG_ADDR, u32 setN_ADDR, u32 currCount_addr, u32 Done_addr, u32 RAM_BASEADDR, u32 BINS);

int Histo_SetCounts(histo *ptr, u32 N);

int Histo_RSTCounter(histo *ptr);

u32 Histo_Get_Curr_Counts(histo *ptr);

int Histo_count_enable(histo *ptr, u32 en);

int Histo_is_done(histo *ptr);

int Histo_rfm(histo *ptr, u32 *trans_buf);

int clean_TDPR(histo *ptr);

#endif /* SRC_HISTOGRAM_H_ */
